<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S4</title>
</head>
<body>

	<h3>Building</h3>
	<p>The name of the building is <?php echo $building->getName(); ?>.</p>
	<p>The <?php echo $building->getName(); ?> has <?php echo $building->getFloor(); ?> floors.</p>
	<p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>.</p>
	<?php $building->setName('Caswyn Complex'); ?>
	<p>The name of the building has been changed to <?php echo $building->getName(); ?>.</p>

	<h3>Condominium</h3>
	<p>The name of the condominium is <?php echo $condominium->getName(); ?>.</p>
	<p>The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getFloor(); ?> floors.</p>
	<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress() ?>.</p>
	<?php $condominium->setName('Enzo Tower'); ?>
	<p>The name of the condominum has been changed to <?php echo $condominium->getName(); ?>. </p>



</body>
</html>